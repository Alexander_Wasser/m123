
[TOC]

# DNS
Das ```Domain Name System (DNS)``` ist ein wichtiger BEstandteil des Internets. Er hat die Aufgabe ```menschenfreundliche Domainnamen``` in ```IP-Adressen``` umzuwandeln. 

### Wie funktioniert DNS?
1. **Domainnamen:** Menschen verwenden leicht merkbare Domainnamen wie ```example.com```, um auf Websites zuzugreifen. Diese Namen sind einfach zu merken als IP-Adressen.

2. **DNS-Anfrage:** Wenn man dann diesen Domainnamen eingibt, sendet der ```Client``` eine ```DNS-Anfrage``` an den ```DNS-Server```. Der ```DNS-Server``` kann ein ```DNS-Server``` des z.B Internetanbieters etc. sein oder ein ```öffentlicher DNS-Server``` von z.B ```Google (8.8.8.8)```sein. 

3. **Namensauflösung:** Der ```DNS-Server``` übersetzt den ```Domainnamen``` in die ```entsprechende IP-Adresse```. Diese ```IP-Adresse ``` wendet dann der ```Client``` an, um eine ```Verbindung``` herzustellen. 

### Warum ist DNS wichtig? 

- **Benutzerfreundlichkeit:** ```DNS erleichtert``` den ```Zugriff auf Websites```, da ```Benutzer``` sich leichter ```Namen```, statt ```IP-Adressen``` merken können. 

- **Skalierbarkeit:** ```DNS ermöglicht``` eine ```einfache Verwaltung``` von Millionen von ```Domainnamen```, ohne dass Benutzer sich um ```IP-Adressen``` kümmern müssen.

## DNS-Server aufsetzen

Wir setzen nun einen Windows DNS-Server auf. Dazu müssen wir 2 neue VMS's erstellen. 1mal einen Client und 1mal einen Windows Server. Dadurch, dass wir schon mal einen Client aufgesetzt habe, üperspringe ich das aufsetzen, des Clients. Zudem müssen wir noch in den Client und Server die M117_Lab Netzkarte einsetzen. Wie man das macht, sollte eigentlich jeder wissen.  

### Windows Server aufsetzen

Um einen Windows Server aufzusetzen müssen wir zuerst ein [Windows Server 2022 Standard Edition](https://portal.azure.com/?Microsoft_Azure_Education_correlationId=dbad2451-956e-4fcd-a7e9-daa23c76db0c#view/Microsoft_Azure_Education/EducationMenuBlade/~/software) haben. Sobald wir das heruntergeladen haben, können wir die VM aufsetzen. Es ist dabei wichtig, bei der Version den ```Windows Server 2022``` auszuwählen (Siehe Bild).
<img src="./Images/Windows Server.png" alt="drawing" width="300px"/>


Nun können wir unseren Server starten. 

Beim ersten Screen, welcher aufplopt, müssen wir folgende Einstellungen einstellen. 
<img src="./Images/Settings.png" alt="drawing" width="300px"/>

Dannach dauert es eine Weile.


Nun müssen wir das Operating System auswählen. Dadurch, dass wir nicht mit dem Terminal, sondern mit der grafischen Oberfläche arbeiten wollen. Um dies zu ermöglichen, wählen wir ```Windows Server 2022 Standard (Desktop Experience)```. 
<img src="./Images/GUI.png" alt="drawing" width="300px"/>

Dannach kommt die Installation. Die ist gleich wie bei einem Client. Deswegen werde ich sie hier nicht erwähnen. 

Als nächstes wird man aufgefordert, ein Passwort festzulegen. Hier habe ich einfach ```Pa$$word``` gewählt. 
<img src="./Images/Passwort.png" alt="drawing" width="300px"/>

Dannach kann man sich normal einloggen. 

### VMware Tools installieren
Nun müssen wir auf dem Server und den Client VMware Tools installieren. Dies haben wir auch schon oft gemacht, weshalb ich es nicht erläutern werde.

### IP-Adresse vergeben
Man muss nun die IP-Adressen vergeben.

#### Server
Auf dem Server gehen wir in die Adapter Einstellungen und gehen dort auf die Eigenschaften des ```Adapter 1```. Dort klicken wir dann auf die Eigenschaften des ```IPv4```:
<img src="./Images/IPv4.png" alt="drawing" width="300px"/>

legen wir seine IP-Adresse und seinen DNS-Server fest. Als IP-Adresse wählen wir ```192.168.245.130``` und als DNS-Server ```8.8.8.8```. Diese DNS-Adresse ist die des öffentlichen Google-Servers. 

Wenn man das alles richtig gemacht hat, sollte es so aussehen:
<img src="./Images/IPv4 Server.png" alt="drawing" width="300px"/>

#### Client
Beim Client gehen wir genau wie beim Server in die ```Adapter 1``` Einstellungen. Dann dort wieder in die ```IPv4 Einstellungen```. Dort geben wir die IP-Adresse ```192.168.245.150``` und beim DNS-Server die IP-Adresse unseres Servers. Das wäre ```192.168.245.130```. Das sollte dann so aussehen. 
<img src="./Images/IPv4 Client.png" alt="drawing" width="300px"/>

### DNS-Server installieren/einrichten
Nun richten wir unseren Server so ein, dass er auch ein DNS-Server ist

Dazu müssen wir die App ```Server Manager``` aufrufen. Dort müssen wir dann auf ```Manage``` und dann auf ```Add Roles and Features```

Dann ploppt ein Fenster auf. Da wählen wir folgendes: <img src="./Images/Server einrichten 1.png" alt="drawing" width="300px"/>

Dann müssen wir unseren Server auswählen: <img src="./Images/Server einrichten 2.png" alt="drawing" width="300px"/>

Hier müssen wir dann den DNS-Server auswählen: <img src="./Images/DNS Server.png" alt="drawing" width="300px"/>

Dann müssen wir unserem Server Features zuweisen: <img src="./Images/Server Features.png" alt="drawing" width="300px"/>

Dann müssen wir das ```Client for NFS``` Feature auswählen. Dann müssen wir auf ```Next``` klicken. <img src="./Images/Server Features 2.png" alt="drawing" width="300px"/>

Dann müssen wir unseren Server installieren: <img src="./Images/Server installieren.png" alt="drawing" width="300px"/>

Jetzt wird unser Server installiert. 

#### Zonen
Um die A-Records zu erstellen müssen wir wieder in die App ```Server Manager```. Dann müssen wir rechts oben auf ```Tools``` und dann auf ```DNS```.

Dann kommen wir auf diesen Screen: <img src="./Images/DNS Tools.png" alt="drawing" width="300px"/>

Jetzt erstellen wir eine neue Zone. Dafür müssen wir einen ```Linksklick``` auf das Win-Server Icon machen. Dann wählen wir die Option ```New Zone aus.```

Wenn alles erfolgreich war, sollte dieses Fenster aufploppen. <img src="./Images/New Zone.png" alt="drawing" width="300px"/>

Dort müssen wir dann eine neue ```Primary Zone``` erstellen. <img src="./Images/New Zone 1.png" alt="drawing" width="300px"/>

Wir wollen ine neue ```Forward lookup zone``` erstellen. <img src="./Images/New Zone 2.png" alt="drawing" width="300px"/>

Als Namen können wir einen beliebigen Namen wählen: <img src="./Images/New Zone 3.png" alt="drawing" width="300px"/>

Als nächstes können wir einfach ```Next``` drücken und dannach ```Finish```. 

Jetzt müssen wir noch einen ```Reverse lookup zone``` ertellen.

Das zu erstellen ist fast gleich wie beim ```Forward lookup zone```.

Wir wählen nun statt ```Forward lookup zone``` den ```Reverse lookup zone```. <img src="./Images/Reverse lookup zone.png" alt="drawing" width="300px"/>

Jetzt wählen wir die  ```IPv4-Adressen``` Option aus: <img src="./Images/IPv4 Zone.png" alt="drawing" width="300px"/>

Dort geben wir folgende Netz-ID ein ```192.168.245``` : <img src="./Images/Netz ID.png" alt="drawing" width="300px"/>

Dann können wir wieder ```Next``` spammen und dann auf ```Finish``` klicken.

#### Records 
Jetzt erstellen wir neue ```A-Records```. 

Dafür machen wir einen ```Linksklick``` auf den ```Unterordner der Forward Lookup Zone``` Ordner und wählen dann ```New Host``` aus. 

Dann sollte dieses Fenster aufploppen: <img src="./Images/New Host.png" alt="drawing" width="300px"/>

Jetzt erstellen wir jeweils einen Host für den Client und den Server. Fangen wir mit dem Client an.

Wir müssen die IP-Adressse des Clients eingeben. Die wäre ```192.168.245.150``` Es sollte nun so aussehen: <img src="./Images/Client Host.png" alt="drawing" width="300px"/>

Das gleiche machen wir nun mit dem Server. Einfach mit der IP-Adresse ```192.168.245.130```. Das sollte dann so aussehen. <img src="./Images/Host Server.png" alt="drawing" width="300px"/>

Jetzt müssen wir noch den Google-DNS-Server hinzufügen, da wir ja nicht für alle Webseiten einen A-Record erstellen wolllen. 

Dafür machen wir wieder einen ```Linksklick``` auf unseren Windows-Server. Dann auf ```Properties```, dann auf ```Forwarders```, dann auf ```Edit```, dann müssen wir ```dns.google``` eingeben. Dies sollte dann so aussehen: <img src="./Images/Google DNS.png" alt="drawing" width="300px"/>

### Test

Nun können wir beim Client mit ```nslookup``` testen. Falls der test erfolgreich war, sollte diese Antowort kommen. <img src="./Images/Test nslookup.png" alt="drawing" width="300px"/>

Das gleiche testen wir jetzt mir der IP-Adresse. Dafür geben wir ```192.168.245.130``` ein. Dann sollte diese Antowrt kommen: <img src="./Images/ip-adresse test.png" alt="drawing" width="300px"/>

Jetzt gucken wir noch, ob unser ```dns.google Forwarder``` funktionert. Deshalb geben wir ```20min.ch``` ein. Das sollte dann bei erfolgreichen Forward so aussehen: <img src="./Images/20min-test.png" alt="drawing" width="300px"/>

Der letzte Test wäre, ein einfacher Ping-Test. Dafür pingen wir einmal 20Min und dann den DNS-Server von Google. Die Befehle wären ```ping 20min.ch``` und ```ping dns.google```. Bei erfolgreichem Ping sollte es dann so aussehen: <img src="./Images/letzter test.png" alt="drawing" width="300px"/>

### Ende

Probleme gab es keine. Ich fand die Aufgabe einen DNS-Server aufzusetzen nicht so schwer. 

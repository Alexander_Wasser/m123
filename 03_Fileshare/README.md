
[TOC]

# Fileshare (Samba)

Samba ist eine Software, welche Netzwerkprotokolle von Windows ```(SMB/CIFS)``` koordiniert, um Dienste wie Datei- und Druckerfreigabe zwischen verschiedenen Betriebssystemen inerhalb eines Netzwerks ermöglicht. Das beste an Samba ist, dass es Windows-Clients auf Dateisystemen und Ressourcen eines Unix/Linux-Server den zugriff ermöglicht und umgekehrt genauso. 

### Wie funktioniert Samba?
1. **Protokoll-Emulation:** Samba stellt die ```SMB/CIFS-Protokolle``` nach, was bedeutet, dass es die Art und Weise ```nachahmt```, wie ```Windows-Systeme``` im Netzwerk ```kommunizieren```. Das ermöglicht ```Computern```, welche unter ```Windows``` laufen, mit ```Unix- oder Linux-Servern``` zu ```kommunizieren```, als ob sie ebenfalls Windows-Systeme wären. 

2. **Dateifreigabe:** Samba ermöglicht es einem ```Linux- oder Unix-Server```, ```Dateisystem-Ressourcen``` wie ```Ordner``` und ```Dateien``` im Netzwerk ```freizugeben```. ```Windows-Clients``` können dann auf diese Ressourcen ```zugreifen```. Es ist so, als ob der ```Liux- oder Unix-Server``` ein ```Windows-Server``` wäre.

3. **Druckerfreigabe:** Samba kann auch den ```gleichen Prozess``` mit ```Druckern``` machen.

4. **Authentifizierung und Autorisierung:** Samba inteagiert mit ```Windows-Domänencontroller-Protokollen```. Somit ```ermöglicht``` es eine ```Authentifizierung``` und ```Autorisierung``` von ```Benutzern```. 

5. **NrtBIOS-Name Resolution:** Samba unterstützt ```TCP/IP (NBT)```, ein älters ```Protokoll```, welches wie ein ```DNS-Server``` agiert. 

## Linux-Samba-Server aufsetzen



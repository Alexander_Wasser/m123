
[TOC]

# DHCP
DHCP steht für **Dynamic Host Configuration Protocol**. Es ermöglicht die automatische Konfiguration von Netzwerkeinstellungen, indem es IP-Adressen, Subnetzmasken, Gateway-Adressen und DNS-Server-Informationen automatisch and die Geräte im Netzwerk verteilt. Das alles entsteht durch den DHCP-Server, welcher meistens im Router mit konfiguriert ist oder ein externes Gerät/Server ist. 

Dieser ganze Prozess beginnt, sobald ein Gerät(Client) dem Netzwerk beitritt und eine IP-Adresse benötigt. Dieser Client sendet dann eine DHCP-Anfrage an das Netzwerk und der DHCP-Server antwortet mit den erforderlichen Informationen. Sobald, ein Client eine IP-Adresse von einem DHCP-Server überbekommt wird dieser ganze Prozes als DHCP-Lease bezeichnet, da die zugewiesenen Konfigurationen eine bestimmte Laufzeit haben(je nach DHCP-Server konfigurationen).

Ein Netzwerk mit einem DHCP-Server hat den grossen Vorteil, dass die ganze Zuweisung der IP-Adresse automatisch verläuft. Heisst, dass somit weniger Konflikte zwischen IP-Adressen entstehen können.

 ## DHCP-Dienst anwenden und verstehen 
 Zuerst muss man das Filius-File ([Download](./Anlagen/DHCP_Dienst_Filius_Vorlage_CAL.fls)) von der Lernumgebung herunterladen. 

 
 Dannach öffnet man das File und sieht folgendes:
 ![Einstiegsbild Fillius](./Images/Einstiegsbild%20Fillius.png)

 Man sieht sofort, dass etwas fehlt. Man Muss Kabelverbindungen ziehen. Es sollte dann wie folgt aussehen: 
 ![Kabelzug](./Images/Kabelzug%20Fillius.png)

 Im Auftag steht, dass der DHCP Server so konfiguriert werden muss, dass die Range von 
 192.168.0.150 - 192.168.0.170 /24 beträgt.

 Client 1 und 2 müssen die IP's vom DHCP Server beziehen. Während Client 3 eine statische IP-Adresse hat. Diese müssen wir mit der MAC-Adresse konfigurieren. 

 Beginnen wir mit dem DHCP Server. Dieser können wir wie folgt konfigurieren, damit er den Clients 1 und 2 IP-Adressen zuweist: 
 
 - **1. Schritt:** Man klickt wählt den DHCP Server an und klickt auf den Button **DHCP- Server einrichten**

 - **2. Schritt:** Man kann in der Rot angezeichneten Box (siehe Bild) die Range definieren. Wir müssen die erste IP-Adresse (erste Box) auf **192.168.0.150** setzen. Die 2. IP-Adresse (2. Box) können wir dann auf **192.168.0.170** setzen. Wenn die Subnetzmask (3. Box) auf **255.255.255.0** gesetzt ist, haben wir somit auch das **/24** definiert. 

 - **3. Schritt:** Jetzt muss man nur noch den DHCP Server aktivieren. 
 ![Bild](./Images/Konfiguration%20DHCP%20Server.png)
 
 Nun haben wir den DHCP-Server so konfiguriert, dass er den Clients 1 und 2 die IP-Adresse zuweist. 

 Nun müssen wir die Clients 1 und 2 so konfigurieren, damit sie die IP-Adresse vom DHCP-Server holen:
 
 - **1.Schritt:** Client 1 auswählen und dann das Häckchen reinmachen bei **"DHCP zur Konfiguration verwenden"**. 
 ![Häkchen](./Images/Konfiguration%20Client%201.png)
 
 - **2.Schritt:** Jetzt muss man genau das Gleiche wie bei Schritt 1 bei **Client 2** anwenden.

 Nun beziehen die Clients 1 und 2 die IP-Adresse vom DHCP-Server.

 Jetzt müssen wir nur noch den Server so konfigurieren, dass er dem Client 3 immer die gleiche IP-Adresse gibt. Dies macht man normalerweise bei Druckern oder andere wichtigen Geräten, worauf die Clients immer Zugriff haben müssen. 

 Dies machen wir mit der MAC-Adresse des Clients. Und zwar wie folgt: 

 - **Schritt 1:** Wir öffnen die Konfiguration des DHCP-Servers mit dem Button **DHCP-Server einrichten**.

 - **Schritt 2:** Wir wechseln zum Tab "Statische Adresszuweisung. 

 - **Schritt 3:** Nun geben wir im 1. Spalten die MAC-Adresse des Clients ein. Im 2. Spalten können wir die IP-Adresse festlegen, welche der Client bekommen soll. 

 - **Schritt 4:** Nun klicken wir auf "Hinzufügen" und der DHCP Server weist dann dieser MAC-Adresse diese IP-Adresse zu. 
 ![Client 3 einrichten](./Images/Client%203%20einrichten.png)

 Anschliessend können wir es überprüfen, indem die MAC-Adresse, sowie die zugehörige IP-Adresse unten in der Tabelle erscheinen:
 ![Überprüfung Client 3](./Images/Überprüfung%20Client%203.png)

 Nun müssen wir dem Client 3 auch noch sagen, dass er die IP_Adresse beim DHCP-Server holen soll. Dies macht man wie bei den anderen 2 Clients, indem wir wieder das Häkchen rein machen. Dies sollte dann so aussehen: 
 ![Client 3](./Images/Client%203.png)


 Nun können wir testen ob es funktioniert. Dies machen wir indem wir auf den grünen Playbutton, oben in der Leiste klicken. 
 ![Playbutton](./Images/Playbutton.png)

 Dannach sehen wir wie die Kabel grün werden. Dies bedeutet, dass der Testvorgang läuft. Wir können die genauen Daten mit Rechtsklick  auf den DHCP Server, einsehen:
 ![Datenaustasusch](./Images/Datenaustausch.png)

 Nun wollen wir wissen ob es funktioniert hat. Vor uns haben wir nun folgende Situation. Wir sehen mehrere grüne, sowie blaue Zeilen. Das wichtigste ist, dass wir diese 3 Zahlen in der blauen Zeile sehen. Dann wissen wir, dass der DHCP-Server diese IP-Adressen vergeben hat. 
 ![DHCP Test](./Images/DHCP%20Test.png)

 Jetzt sehen wir aber noch andere Zeilen. In der ersten blauen Zeile steht **DHCP Discover**. Das bedeutet, dass der Client ein Discover-Paket ins Netzwerk, an alle verfügbaren Geräte, sendet um einen DHCP-Server zu finden. 

 Sobald es einen gefunden hat antowrtet der DHCP-Server mit **DHCP-Offer**. Heisst, dass der DHCP-Server dem Client freie IP-Adressen sozusagen offeriert. 

 Der Client wählt eine beliebige IP-Adresse aus und sendet dann eine **DHCP-Request** Nachricht an den Server um eine Bestätigung zu bekommen ob diese noch frei ist. Falls Ja, aktiviert der Server diese IP-Adresse für den Client. Falls nicht, wählt der Client eine neue aus. 

 Am Schluss sendet der DHCP-Server einen **DHCP-ACK** an den Client. Dieser dient als Bestätigung, damit der Client weiss, dass der Server seine IP-Adresse aktiviert hat. 


 


 ## DHCP mit Cisco Packet Tracer

 ### Ausgangslage
 ![Ausgangslage](./Images/Ausgangslage.png)

 Im nachfolgenden Netzwerk sind allen Geräte ausser PC3 per DHCP eine IPv4 Adresse zugewiesen worden. Der Router R1 agiert als DHCP Server und verteilt IP Adressen.

*Tipp: Starten Sie alle Geräte nach dem Öffnen des Labors neu. Drücken Sie dazu unten Links im Packet Tracer auf das nachfolgende Symbol:![reset](./Images/reset.png)*

Anschliessend empfiehlt es sich 1 Minute zu warten, bevor mit den Aufgaben fortgefahren wird, damit alle Geräte Zeit haben per DHCP eine IPv4-Adresse zu beziehen.
 
### 1. Router-Konfiguration auslesen
Im ersten Schritt gilt es herauszufinden in welchem Subnet und welchem Range der DHCP Server IPv4-Adressen im lokalen Netzwerk verteilt. Dafür gibt verschiedene Befehle, die in Ihrem Output die entsprechenden Optionen beinhalten. Die Befehle können in der CLI des Routers ausgeführt werden.

Damit die Befehle ausgeführt werden können, muss sich der Router im richtigen Modus befinden. Öffnen Sie die CLI und drücken Sie solange die Enter Taste bis ```R1>``` oder ähnlich erscheint. Der Router ist nun bereit für ihre Befehle.

<img src="./Images/CLI.png" alt="drawing" width="400px"/>

Das ```>``` Zeichen in ```R1>``` zeigt dem Administrator an, dass er sich im ```User Mode``` befindet. Ist dies der Fall, wechseln Sie in den ```Enabled Mode``` indem sie den Befehl ```enable``` eingeben und anschliessend Enter drücken. Ob erfolgreich in den ```enabled```-Mode gewechselt wurde, sehen Sie daran, dass nun das ```#```-Symobol in der Befehlzeile steht.
<img src="./Images/enable.png" alt="drawing" width="400px"/>

Probieren Sie die nachfolgenden Befehle aus und versuchen Sie die nachfolgenden Fragen zu beantworten:

- Für welches Subnetz ist der DHCP Server aktiv?
- Welche IP-Adressen sind vergeben und an welche MAC-Adressen? (Antwort als Tabelle)
- In welchem Range vergibt der DHCP-Server IPv4 Adressen?
- Was hat die Konfiguration ```ip dhcp excluded-address``` zur Folge?
- Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?

Befehle:

```show ip dhcp pool```

```show ip dhcp binding```

```show running-config```

### Für welches Subnetz ist der DHCP Server aktiv?

Mit dem Befehl ```show ip dhcp pool``` zeigt der DHCP Server folgendes an: 

<img src="./Images/Aufgabe 1.png" alt="drawing" width="500px"/>

Anhand dessen sehen wir, dass er im **/24 Subnetz** ist, da er **254 IP-Adressen** vergeben kann. Bei einem Bei einem ```/25 Subnetz```, könnte er z.B nur ```126 IP-Adressen``` verteilen. 

<img src="./Images/Subnetze.png" alt="drawing" width="400px"/>

### Welche IP-Adressen sind vergeben und an welche MAC-Adressen?

Dies sehen wir anhand dieses Befehls ```show ip dhcp binding``` herausfinden. Nun sehen wir folgendes. 
<img src="./Images/IP Binding.png" alt="drawing" width="600px"/>

Mit dieser Info können wir nun unsere Tabelle erstellen. 


| MAC-Adresse    | IP-Adresse    |
|----------------|---------------|
| 0009.7CA6.4CE3 | 192.168.36.31 |
| 0007.ECB6.4534 | 192.168.36.35 |
| 0001.632C.3508 | 192.168.36.32 |
| 00E0.8F4E.65AA | 192.168.36.34 |
| 0050.0F4E.1D82 | 192.168.36.33 |
| 00D0.BC52.B29B | 192.168.36.36 |

### In welchem Range vergibt der DHCP-Server IPv4 Adressen?

Dies können wir wieder mit dem Befehl ```show ip dhcp pool``` herausfinden. 
<img src="./Images/Range IPv4.png" alt="drawing" width="600px"/>
Nun sehen wir den IP address range. Daraus können wir herauslesen, in welchem Range der DHCP-Server die IP-Adressen vergibt. Dies wäre von ```192.168.36.1``` bis ```192.168.36.254```

### Was hat die Konfiguration ```ip dhcp excluded-adress``` zur Folge?

Die ```ip dhcp excluded-adress``` bedeutet, dass diese Adressen für statische IP-Adressen reserviert sind. Diese werden oftmals für Drucker etc. gebraucht. Man kann herausfinden wie viele IP-Adressen "excluded" sind, wenn man in das Terminal ```show running-config``` eingibt. Dann bekommt man folgendes Bild. 

<img src="./Images/excluded Adress.png" alt="drawing" width="600px"/>

Anhand dieses Bildes erkennt man, dass die IP-Adressen ```192.168.36.1```, ```192.168.36.30```, ```192.168.26.176``` und die ```192.168.36.241``` "excluded" sind.

### Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?

Diese Antwort können wir mit folgendem Befehl beantworten ```show ip dhcp pool```. Mit diesem Befehl, können wir sehen wie viele IP-Adressen "excluded" und wie viele IP-Adressen wir dynamisch vergeben können. Dies sehen wir anhand folgenden Beispiels: 
<img src="./Images/dynamische Adressen.png" alt="drawing" width="600px">

Nun sehen wir folgende Sachen:
   
    - ```Total adresses``` bedeutet wie viele Adressen der DHCP-Server total hat.
    
    - ```Leased adresses``` bedeutet wie viele Adressen den bereits geleast sind.
    
    - ```excluded adresses``` hier sehen wir wie viele Adressen "excluded" sind.

Nun müssen wir die Anzahl ```Total adresses``` - die Anzahl ```exluded adresses``` rechnen und dann kommen wir auf das Ergebnis. 
Sprich bei diesem Beispiel: 
```254-2=252```

Heisst unser DHCP-Server verteilt 252 IP-Adressen dynamisch.

## 2. DORA - *DHCP Lease* beobachten

Mit dem *Cisco Packet Tracer* können Ethernet Frames oder allgemeiner *Protocol Data Unit (PDU)* sehr gut nachverfolgt werden. In diesem Schritt soll eine DHCP IPv4-Adress-Zuteilvorgang mitverfolgt werden.

1. *PC2* dazu bringen eine neuen DHCP-Request zu starten. Dafür im Menü des PCs unter *Config => FastEthernet0* die IP Konfiguration kurz von *DHCP* auf Static setzen.
2. Unten rechts im *Packet Tracer* auf *Simulation* wechseln.
3. Im Menü des *PC2* unter *Config => FastEthernet0* die IP Konfiguration kurz von *Static* wieder auf *DHCP* setzen.
<img src="./Images/poweroff.png" alt="drawing" width="300px"/>

4. *PC2* wieder starten.
5. Es erscheint sofort ein gelbes PDU beim PC2 (bzw. beim PC bei dem diese Schritte gerade ausgeführt werden).
<img src="./Images/pcwithdhcpdiscover.png" alt="drawing" width="300px"/>

6. Mit einem *klick* kann das *PDU* geöffnet und analysiert werden.
<img src="./Images/packettracer_ms_doku.png" alt="drawing" width="300px"/>

*Hinweis:* Teilweise verschwindet der DHCP-Discover sobald das *Properties-Window* des Gerätes geschlossen wird: Lassen Sie das *Properties-Window* einfach offen.

Unter *Outbound PDU Details* finden sehen wir den hierarchischen Aufbau des PDUs mit den verschiedenen Layern. Unterhalb von *IP* ist der *DHCP* Layer zu finden. Mithilfe der [Microsoft DHCP](https://learn.microsoft.com/en-us/windows-server/troubleshoot/dynamic-host-configuration-protocol-basics) Informationsseite können wir herausfinden, dass es sich hierbei um einen DHCP Discover handelt, da der *OP Code* den Wert 1 hat.

Ziel ist es nun alle vier PDUs zu finden und zu identifzieren.

Fragen:

- Welcher OP-Code hat der DHCP-Offer?
- Welcher OP-Code hat der DHCP-Request?
- Welcher OP-Code hat der DHCP-Acknowledge?
- An welche IP-Adresse wird der DHCP-Discover geschickt? Was ist an dieser IP-Adresse speziell?
- An welche MAC-Adresse wird der DHCP-Discover geschickt? Was ist an dieser MAC-Adresse speziell?
- Weshalb wird der DHCP-Discover vom Switch auch an PC3 geschickt?
- Gibt es ein DHCP-Relay in diesem Netzwerk? Wenn ja, welches Gerät ist es? Wenn nein, wie kann das mithilfe der DHCP-PDUs festgestellt werden?
- Welche IPv4-Adresse wird dem Client zugewiesen?

Auf den Screenshots sollten alle Felder von DHCP sichtbar sein:

- Screenshot des DHCP-Discovers PDUs
- Screenshot des DHCP-Offers PDUs
- Screenshot des DHCP-Request PDUs
- Screenshot des DHCP-Acknowledge PDUs

### Welcher OP-Code hat der DHCP-Offer?

## 3. Netzwerk umkonfigurieren

Aktuell erhält der Server seine IPv4-Adresse vom DHCP-Server. Diese IPv4-Adresse ist nicht statisch zugewiesen und kann sich zum Beispiel beim Neustart des Routers oder des Servers ändern. Grundsätzlich kann der Router auch nach Ablauf der *DHCP lease time* eine andere IPv4-Adresse zuteilen.
Um das zu verhindern weisen wir dem Server eine statische IPv4-Adresse zu.

**Aufgabe**

Dem Server eine statische IPv4-Adresse zu weisen. Nehmen Sie eine IPv4-Adresse, die sich leicht merken lässt.

**Hinweise**

- Die statisch zugewiesene IPv4-Adresse muss ausserhalb des DHCP-Bereichs liegen. Konsultieren Sie hierzu Ihre Antworten aus Aufgabe 1.
- Die IPv4-Adresse im Menü des Servers unter *Config -> FastEthernet0/0* konfiguriert werden.
- Das Gateway kann in Menü unter Config -> Settings festgelegt werden.

Im Laborbericht sind folgende Screenshots zu hinterlegen:

- Screenshot Konfigurationsmenü Server mit Sichtbarer IPv4-Adresse vom Interface FastEthernet
- Screenshot Konfigurationsmenü Server mit Sichtbarer IPv4-Adresse des Gateways
- Screenshot der Webseite des Servers auf einem der PCs inkl. sichtbarer IPv4-Adresse des Servers

Prüfen Sie anschliessend, ob der Server von PC1 erreichbar ist.

- ```ping 192.168.X.X``` (IP-Adresse des Servers einsetzen) unter *Desktop -> Command Prompt*

Auf dem Server ist eine Webseite aktiv. Mit dem Webbrowser zugreifen *Desktop -> Web Browser*


## DHCP Server zur Verfügung stellen. 

Ich habe mich an der Anleitung dieses Schülers gehalten: [Anleitung](https://gitlab.com/ser-cal/m123-lernprodukt/-/blob/main/01_DHCP/README.md)

### Troubleshooting

Hier dokumentiere ich meine Troubleshootings und wie ich jeweils vorgegangen bin. 

#### Troubleshooting 1

Ich hatte während den Lektionen das Problem, dass sich der Server die ganze Zeit aufhängt mit der Meldung ```watchdog: BUG: soft lockup - CPU#1 stuck for 23s```

Während meiner Recherche, konnte sich keiner meiner Quellen einig sein. Einige meinten es hänge mit einem Windows Update zusammen, andere meinten wiederum es hänge mit der BIOS zusammen, andere meinten es hänge mit dem RAM und wiedrrum andere meinten die CPU hätte zu wenig Cores.

Einer dieser Quellen meinte, die Ursache für das ist, dass die CPU anscheinend in einen Loop läuft und keine Daten mehr verarbeitet. Diese würden dann in eine Warteschlange laufen, was schlussendlich zum Lockup führt. 

Lösungsversuche:
- Mehr RAM der VM zuwgewiesen (Mehr Kerne konnte ich nicht zuweisen). 
- Client gerestarted.
- Laptop neu gestartet.
- Laptop an den Strom angeschlossen.
- VMware als Admin gestartet. 

Keiner dieser Lösungen funktionierten, also blieb mir nicht andres mehr übrig, als einen neuen Server aufzusetzen.  
Ich gehe davon aus, dass es glückssache ist, ob diesen BUG jemanden heimsucht oder nicht


#### Troubleshooting 2

Ich konnte nun mit dem neuem Server bis zu den ```IP-Adressen Pool's``` konfigurieren. 

Dannach suchte mich wieder der CPU-Lockup BUG heim. Ich habe meinen Server neuegestartet, dieser hang sich jedoch wieder nach dem Neu Start, nach kurzer Zeit wieder auf. 

Da ich vermute, dass das mit dem CPU-Lockup zusammenhängt, ist meine einzige Lösung eine neue VM aufzusetzen. 

